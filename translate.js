var new_lang = {

    "lt0": "Preț vechi:",

    "lt1": "RON",

    "lt2": "Reducere",

    "lt3": "53%",

    "lt4": "Preț nou:",

    "lt5": "RON",

    "lt6": "Până la sfârșitul promoției au rămas:",

    "lt7": ":",

    "lt8": ":",

    "lt9": "Au mai rămas doar 9 buc. la promoție!",

    "lt10": "Galerie foto",

    "lt11": "Putere de mărire 16x, lentile de sticlă, rezistent la apă și rezistent la șoc",

    "lt12": "Caracteristicile monocularului",

    "lt13": "Putere de mărire:",

    "lt14": "16x",

    "lt15": "Câmp de vedere:",

    "lt16": "66 m/8000 m",

    "lt17": "Imagine:",

    "lt18": "calitativă, clară",

    "lt19": "Diametru obiectiv:",

    "lt20": "52 mm",

    "lt21": "Intervalul de temperatură de funcționare:",

    "lt22": "de la -40 la +50",

    "lt23": "Greutate:",

    "lt24": "270 g",

    "lt25": "Optica:",

    "lt26": "sticlă",

    "lt27": "Carcasă:",

    "lt28": "metalică cauciucată",

    "lt29": "Caracteristicile monocularului",

    "lt30": "<img alt=\"monocular Bushnell\" src=\"images/f1.jpg\" style=\" width:150px; border-radius:50%; margin-bottom:15px;\" title=\"monocular Bushnell\"><br>Rezistent la apă",

    "lt31": "<img alt=\"monocular  Bushnell\" src=\"images/f2.jpg\" style=\"width:150px; border-radius:50%; margin-bottom:15px;\" title=\"monocular  Bushnell\"><br>Intervalul de temperatură de funcționare -40+50",

    "lt32": "<img alt=\"monocular Bushnell\" src=\"images/f3.jpg\" style=\" width:150px; border-radius:50%; margin-bottom:15px;\" title=\"monocular Bushnell\"><br>Ajustare dublă",

    "lt33": "<img alt=\"monocular Bushnell\" src=\"images/f4.jpg\" style=\"width:150px; border-radius:50%; margin-bottom:15px;\" title=\"monocular Bushnell\"><br>Carcasă cauciucată",

    "lt34": "Prezentare video a monocularului",

    "lt35": "Recenziile clienților",

    "lt36": "Matei Teodorescu",

    "lt37": "Sunt un vânător înrăit și vânez diferite vietăți. Uneori, când mergi pe urmele unui animal trebuie să trec prin desișuri și în acest moment este incomod să porți cu tine binocluri mari, așa că am decis să cumpăr un monocular, iar marca am ales-o XX, deoarece produsele sale sunt de înaltă calitate. Am pus multe întrebări managerului despre calitate și caracteristici. Pentru că mi-a plăcut acest model, am făcut o comandă. Când am primit acest monocular și l-am luat în mână, cuvintele managerului s-au adeverit. Calitate excelentă și suprafață cauciucată plăcută la atingere. De asemenea, este rezistent la umiditate, ceea ce este important pentru mine. Am încercat monocularul la vânătoare, datorită dimensiunilor sale, este foarte convenabil și practic în utilizare. Îl folosesc nu numai la vânătoare, îl iau întotdeauna în drumeții și excursii turistice. În general, este potrivit pentru orice activitate în aer liber în natură. Cumpărătura m-a bucurat nespus, am rămas mulțumit. Mulțumesc!",

    "lt38": "Constantin Velu",

    "lt39": "MONOCULARUL VA PLĂCEA TUTUROR IUBITORILOR DE ACTIVITĂȚI ÎN AER LIBER ȘI TURISM!! Suntem pasionați cu prietenii,  de călătorii pe râuri de munte, o ocupație foarte interesantă. Nu poți lua multe lucruri cu tine în barcă. Prin urmare, am optat pentru acest monocular XX, deoarece este compact în comparație cu binoclul, ușor de utilizat și, cel mai important, rezistent la apă. Când albia râului se liniștește, plutim încet și în acest moment mă bucur de frumusețea naturii cu ajutorul monocularului. Am comandat un set de monoculare, iar pentru al doilea mi-au oferit o reducere bună. Se simte o atitudine caldă față de clienți. Nu mi-e rușine să vă recomand prietenilor și cunoștințelor mele. Vă mulțumesc pentru sinceritatea și căldura dvs. în comunicare și pentru sfaturile detaliate. Fac cumpărături periodice din acest magazin în calitate de cadou prietenilor mei, care sunt, de asemenea, pasionați de turism și sunt foarte fericit!",

    "lt40": "Ana",

    "lt41": "Îmi place să observ frumusețea naturii noastre. Și în acest model am văzut exclusivitate în comparație cu alte modele de monoculare. Designul strict, peisajul fascinant datorită calității imaginii și suprafața cauciucată plăcută la atingere mi-au determinat alegerea :). Prin urmare, am decis să comand anume acest monocular. În mediul meu mai multe persoane se interesează de unde poate fi cumpărat. Au rămas încântați de cumpărătură. Preț accesibil și calitate excelentă a mărfurilor. Vă mulțumesc pentru oportunitatea de a cumpăra la cel mai bun preț bunuri de calitate!",

    "lt42": "Livrare și plată",

    "lt43": "Livrarea prin poștă în termen<br>de la 7 la 14 zile lucrătoare.<br>Costul de livrare al produsului este de la 350 RON.",

    "lt44": "Fără plată în avans! Plata comenzilor se face direct la primirea comenzii",

    "lt45": "Aveți dreptul să renunțați la cumpărătură în termen de 14 zile de la primirea comenzii, indiferent de motivul returnării.",

    "lt46": "Preț vechi:",

    "lt47": "RON",

    "lt48": "Reducere",

    "lt49": "53%",

    "lt50": "Preț nou:",

    "lt51": "RON",

    "lt52": "<strong>Lăsați o cerere</strong><br>și vă vom contacta pentru a preciza detaliile comenzii",

    "lt53": "România",

    "lt54": "<a href=\"/politic.html\" style=\"color: inherit;\">Politica de confidențialitate</a>&nbsp;&nbsp;<a href=\"/oferta.html\" style=\"color: inherit;\">Ofertă publică</a><br><br>"
};

function Translater() {
    for (class_name in new_lang) {
        var elements = document.getElementsByClassName(class_name);
        if (elements.length) {
            for (key in elements) {
                elements[key].innerHTML = new_lang[class_name];
            }
        }
    }
};

//Monocular XX ultraputernic compact ușor pentru observare
//magazin online de monoculare
//Monocular XX ultraputernic compact
//pentru observare la pescuit, vânătoarea și în natură
//rezoluție ajustabilă
//distanță ajustabilă
//Introduceți numele și prenumele
//Introduceți telefonul
//Заказать со скидкой - Comandă cu reducere